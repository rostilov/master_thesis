#include "matplotlibcpp.h"
#include <chrono>
#include <memory>
#include <eigen3/Eigen/Dense>
namespace plt = matplotlibcpp;

class SymmetricMatrix {
public:
    SymmetricMatrix() {

    }

    SymmetricMatrix(const std::vector<std::vector<double>>& matrix) : matrix_(matrix) {
        if (matrix.empty()) {
            throw std::runtime_error("Wrong input");
        }
        if (matrix.size() != matrix[0].size()) {
            throw std::runtime_error("Wrong input");
        }
        Eigen::MatrixXd eigen_matrix(matrix.size(), matrix.size());
        for (size_t ind_x = 0; ind_x < matrix.size(); ++ind_x) {
            for (size_t ind_y = 0; ind_y < matrix.size(); ++ind_y) {
                if (matrix.at(ind_x).at(ind_y) != matrix.at(ind_y).at(ind_x)) {
                    throw std::runtime_error("Matrix is not symmetric");
                }
                eigen_matrix(ind_x, ind_y) = matrix.at(ind_x).at(ind_y);
            }
        }

        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> solver(eigen_matrix);
        Eigen::VectorXd eigens = solver.eigenvalues().real();
        for (size_t ind = 0; ind < matrix.size(); ++ind) {
            eigen_values_.push_back(eigens(ind));
        }
        std::sort(eigen_values_.begin(), eigen_values_.end());

        auto inverse = eigen_matrix.inverse();
        inverse_matrix_ = std::vector<std::vector<double>>(matrix.size(), std::vector<double>(matrix.size()));
        for (size_t ind_x = 0; ind_x < matrix.size(); ++ind_x) {
            for (size_t ind_y = 0; ind_y < matrix.size(); ++ind_y) {
                inverse_matrix_.at(ind_x).at(ind_y) = inverse(ind_x, ind_y);
            }
        }
    }

    const std::vector<std::vector<double>>& get() const {
        return matrix_;
    }

    const std::vector<std::vector<double>>& getInverse() const {
        return inverse_matrix_;
    }

    std::vector<double> multiply(const std::vector<double>& vector) const {
        if (vector.size() != matrix_.size()) {
            throw std::runtime_error("Wrong dimensions");
        }
        std::vector<double> result(vector.size(), 0);
        for (size_t ind = 0; ind < vector.size(); ++ind) {
            for (size_t index = 0; index < vector.size(); ++index) {
                result[ind] += matrix_.at(ind).at(index) * vector.at(index);
            }
        }
        return result;
    }

private:
    std::vector<std::vector<double>> matrix_;
    std::vector<std::vector<double>> inverse_matrix_;
    std::vector<double> eigen_values_;
};

class NormalDistributionGenerator {
public:
    NormalDistributionGenerator() :
        generator_(std::chrono::system_clock::now().time_since_epoch().count()),
        distribution_(0, 1) {

    }

    //    NormalDistributionGenerator(int seed, double mean, double stddev) :
    //        generator_(seed),
    //        distribution_(mean, stddev) {
    //    }

    inline double generate() {
        return distribution_(generator_);
    }

    inline double generate(double mean, double stddev) {
        return  stddev * distribution_(generator_) + mean;
        //        std::normal_distribution<double> temp_distrbution_(mean, stddev);
        //        return temp_distrbution_(generator_);
    }

private:
    std::mt19937 generator_;
    std::normal_distribution<double> distribution_;
};

struct BivariateNormalParams {
    double mean_x;
    double mean_y;
    double sigma_x;
    double sigma_y;
    double correlation;
    SymmetricMatrix inverse_cov_matrix;
};

std::vector<double> BivariateNormalGibbsSampler(const BivariateNormalParams& params, const std::vector<double> state,
                                                NormalDistributionGenerator* generator) {
    auto result = state;
    {
        auto mean = params.mean_x + params.correlation * (params.sigma_x / params.sigma_y) * (state.at(1) - params.mean_y);
        auto sigma = (1 - params.correlation * params.correlation) * params.sigma_x * params.sigma_x;
        result.at(0) = generator->generate(mean, sigma);
    }
    {
        auto mean = params.mean_y + params.correlation * (params.sigma_y / params.sigma_x) * (state.at(0) - params.mean_x);
        auto sigma = (1 - params.correlation * params.correlation) * params.sigma_y * params.sigma_y;
        result.at(1) = generator->generate(mean, sigma);
    }
    return result;
}
class BivariateNormalGenerator {
public:
    BivariateNormalGenerator(const BivariateNormalParams& params)
        : params_(params),
          generator_ptr_(std::make_shared<NormalDistributionGenerator>()) {

    }

    std::vector<double> generate() {
        double gen_first = generator_ptr_->generate();
        double gen_second = generator_ptr_->generate();

        return {params_.sigma_x * gen_first + params_.mean_x,
                    params_.sigma_y * (params_.correlation * gen_first + std::sqrt(1 - params_.correlation * params_.correlation) * gen_second) + params_.mean_y};
    }

private:
    const BivariateNormalParams params_;
    std::shared_ptr<NormalDistributionGenerator> generator_ptr_;
};

template <class T>
std::vector<T> operator-(const std::vector<T>& lhs, const std::vector<T>& rhs) {
    auto result = lhs;
    for (size_t ind = 0; ind < lhs.size(); ++ind) {
        result[ind] -= rhs[ind];
    }
    return result;
}

template <class Function>
std::vector<double> calculateContrastiveDivergenceDelta(size_t cd_step_number, const std::vector<double>& data_sample,
                                                        const Function& phi, const BivariateNormalParams& current_params,
                                                        NormalDistributionGenerator* generator) {
    auto sample = data_sample;
    for (size_t ind = 0; ind < cd_step_number; ++ind) {
        sample = BivariateNormalGibbsSampler(current_params, sample, generator);
    }

    return phi(data_sample, current_params) - phi(sample, current_params);
}

std::vector<double> phi(const std::vector<double>& sample, const BivariateNormalParams& params) {
    return params.inverse_cov_matrix.multiply(sample);
//    auto result = sample;
//    result[0] = (sample[0] - 0.5 * sample[1]);
//    result[1] = (-0.5 * sample[0] + sample[1]);

//    for (auto& item : result) {
//        item /= 0.75;
//    }
//    return result;

    //    return std::vector<double>(sample.size());
}




int main()
{
    std::vector<std::vector<double>> cov_matrix;
    cov_matrix.push_back({1 , 0.5});
    cov_matrix.push_back({0.5 , 1});
    auto params = BivariateNormalParams{0, 0, 1, 1, 0.5};
    params.inverse_cov_matrix = SymmetricMatrix(SymmetricMatrix(cov_matrix).getInverse());
    BivariateNormalGenerator gen(params);
    NormalDistributionGenerator generator;

    int n = 1000;
    std::vector<std::vector<double>> dataset;
    for(int i=0; i<n; ++i) {
        dataset.emplace_back(gen.generate());
    }

    auto wrong_params = params;
    wrong_params.mean_x = 5;
    wrong_params.mean_y = 5;
    const double learning_rate = 0.01;
    const size_t cd_m = 4;
    std::vector<std::vector<double>> deltas(dataset.size());
    std::vector<double> x, y;

    for (size_t gd_step = 0; gd_step < 1000; ++gd_step) {
        for (size_t ind = 0; ind < dataset.size(); ++ind) {
            deltas[ind] = calculateContrastiveDivergenceDelta(cd_m, dataset[ind], phi, wrong_params, &generator);
        }

        auto average = std::vector<double>(deltas.front().size(), 0);
        for (const auto& delta : deltas) {
            for (size_t ind = 0; ind < delta.size(); ++ind) {
                average[ind] += delta[ind];
            }
        }

        for (auto& item : average) {
            item /= deltas.size();
        }

        x.push_back(wrong_params.mean_x);
        y.push_back(wrong_params.mean_y);
        std::cout << "Step number : " << gd_step << " Current params " << wrong_params.mean_x << " " << wrong_params.mean_y << std::endl;
        wrong_params.mean_x += learning_rate * average[0];
        wrong_params.mean_y += learning_rate * average[1];
    }

    plt::plot(x, y, "o");
    plt::show();
}


